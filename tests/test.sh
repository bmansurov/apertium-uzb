hfst-lexc apertium-uzb.uzb_Cyrl.lexc -o tests/uzb_Cyrl.lexc.hfst
#hfst-fst2strings tests/uzb_Cyrl.lexc.hfst 

hfst-twolc -R -i apertium-uzb.uzb_Cyrl.twol -o tests/uzb_Cyrl.twol.hfst

hfst-compose-intersect -1 tests/uzb_Cyrl.lexc.hfst -2\
	tests/uzb_Cyrl.twol.hfst  -o tests/uzb_Cyrl.autogen.hfst
#hfst-fst2fst -O -i tests/uzb_Cyrl.autogen.hfst -o tests/uzb_Cyrl.autogen.hfst.ol

hfst-invert -i tests/uzb_Cyrl.autogen.hfst -o tests/uzb_Cyrl.automorf.hfst
hfst-fst2fst -O -i tests/uzb_Cyrl.automorf.hfst -o tests/uzb_Cyrl.automorf.hfst.ol
#hfst-proc tests/uzb_Cyrl.automorf.hfst.ol

#echo "оилам" | hfst-proc tests/uzb_Cyrl.automorf.hfst.ol
#echo "оилаим" | hfst-proc tests/uzb_Cyrl.automorf.hfst.ol

echo "Reading input..."
cat tests/input.txt | hfst-proc tests/uzb_Cyrl.automorf.hfst.ol >\
	tests/test_output.txt

echo "Comparing output..."
diff --color tests/output.txt tests/test_output.txt

echo "Testing done. Cleaning up."

echo " боғ<n><sg><dat>" | hfst-lookup tests/uzb_Cyrl.autogen.hfst
echo " боғ<n><sg><dat>" | hfst-lookup tests/uzb_Cyrl.lexc.hfst

rm tests/uzb_Cyrl.lexc.hfst tests/uzb_Cyrl.twol.hfst\
	tests/uzb_Cyrl.autogen.hfst\
	tests/uzb_Cyrl.automorf.hfst tests/uzb_Cyrl.automorf.hfst.ol\
	tests/test_output.txt
